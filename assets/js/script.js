const RESOLUTIONS = [
  4000,
  3750,
  3500,
  3250,
  3000,
  2750,
  2500,
  2250,
  2000,
  1750,
  1500,
  1250,
  1000,
  750,
  650,
  500,
  250,
  100,
  50,
  20,
  10,
  5,
  2.5,
  2,
  1.5,
  1,
  0.5
];
const EXTENT = [2420000, 130000, 2900000, 1350000];
const PROJECTION = ol.proj.get("EPSG:2056");
PROJECTION.setExtent(EXTENT);

const MAX_SEARCH_RESULTS = 10;

const SENTINEL_NDVI_LAYER = "07_NDVI_DIFF_2017_2018";

let colors = {
  2025: "#610085",
  2024: "#AA2E18",
  2023: "#FF6161",
  2022: "#225522",
  2021: "#0088D3",
  2020: "#004499",
  2019: "#ffcc00",
  2018: "#cc19ff",
  2017: "#ff0000",
  2016: "#ff9933",
  default: "#0000ff"
};

let thresholds = {
  2025: -0.15,
  2024: -0.15,
  2023: -0.15,
  2022: -0.15,
  2021: -0.15,
  2020: -0.15,
  2019: -0.15,
  2018: -0.15,
  2017: -0.15,
  2016: -0.15,
  default: -0.15,
};

let activeLayers = {};

let mapX = 2600000;
let mapY = 1200000;
let mapZoom = 10;

let map;

function getColorForYear(year) {
  if (colors[year]) {
    return colors[year];
  }

  const hue = year * 137.508; // use golden angle approximation
  return colors[year] = hslToHex(hue,100,75);
}

function initialize() {
  getYears().forEach(year => {
    createLegendForYear(year);
  });

  let matrixIds = [];
  for (let i = 0; i < RESOLUTIONS.length; i++) {
    matrixIds.push(i);
  }

  let wmtsLayer = new ol.layer.Tile({
    source: new ol.source.WMTS({
      attributions: [
        new ol.Attribution({
          html:
            '<a target="new" href="https://www.swisstopo.admin.ch/' +
            'internet/swisstopo/en/home.html">swisstopo</a>'
        })
      ],
      url:
        "https://wmts10.geo.admin.ch/1.0.0/{Layer}/default/current/2056/{TileMatrix}/{TileCol}/{TileRow}.jpeg",
      tileGrid: new ol.tilegrid.WMTS({
        origin: [EXTENT[0], EXTENT[3]],
        resolutions: RESOLUTIONS,
        matrixIds: matrixIds
      }),
      projection: PROJECTION,
      layer: "ch.swisstopo.pixelkarte-farbe",
      requestEncoding: "REST"
    })
  });

  map = new ol.Map({
    layers: [wmtsLayer],
    target: "map",
    view: new ol.View({
      center: [2598000, 1201000],
      projection: PROJECTION,
      resolution: 5
    }),
    controls: ol.control
      .defaults({
        attributionOptions: {
          collapsible: false
        }
      })
      .extend([
        new ol.control.ScaleLine({
          units: "metric"
        })
      ]),
    logo: false
  });

  map.on("moveend", function(evt) {
    pushURL();
  });

  readURL();

  document.body.addEventListener("click", handleBodyClick, true);
}

function sentinelWMSLayer(year, threshold = "-0.15") {
  return new ol.layer.Tile({
    extent: EXTENT,
    source: new ol.source.TileWMS({
      url: "https://services.sentinel-hub.com/ogc/wms/bfde1ad0-8438-4ce6-be50-c14c82e6bb67",
      crossOrigin: "anonymous",
      params: {
        LAYERS: SENTINEL_NDVI_LAYER,
        FORMAT: "image/png",
        VERSION: "1.3.0",
        EVALSCRIPT: getSentinelScript(year, threshold),
        TIME: getTimeParamForYear(year)
      },
      serverType: "mapserver",
      projection: "EPSG:4326"
    })
  });
}

/**
 * @param {number} year the year of the changes
 * @param {number} threshold which threshold should be used for displaying change detection
 * @returns {string} the sentinel script base64 encoded
 */
function getSentinelScript(year, threshold = -0.15) {
  const script = `
  //VERSION=3
  const HIGH_COLOR = [${hexToSentinelColor(getColorForYear(year))}];
  const TRANSPARENT_COLOR = [0, 0, 0, 0];

  const HIGH_THRESHOLD = ${threshold};

  function setup() {
    return {
      input: ["B04", "B08"],
      output: { bands: 4 },
      mosaicking: "TILE"
    };
  }

  function ndvic(b1, b2) {
    // Return NDVI of two bands
    return ((b1 - b2) / (b1 + b2));
  }

  function evaluatePixel(samples, scenes) {  
    var t = 0;
    while (scenes[t].date.getFullYear() == scenes[0].date.getFullYear()) {
      t++;
    }

    var max = 0;
    for (var i = 0; i < t; i++) {
      var ndvi = ndvic(samples[i].B08, samples[i].B04);
      max = ndvi > max ? ndvi : max;
    }
    
    var max2 = 0;
    for (i = t; i < samples.length; i++) {
      var ndvi2 = ndvic(samples[i].B08, samples[i].B04);
      max2 = ndvi2 > max2 ? ndvi2 : max2;
    }
    
    var ndviDiff = max - max2;

    if (ndviDiff < HIGH_THRESHOLD) {
      return HIGH_COLOR;  
    }
    
    return TRANSPARENT_COLOR; 
  }

  function preProcessScenes(collections) {
    collections.scenes.tiles = collections.scenes.tiles.filter(function (tile) {
        return (new Date(tile.date) < new Date("${year-1}-08-01T00:00:00Z")) ||
               (new Date(tile.date) >= new Date("${year}-04-01T00:00:00Z"))
    })
    return collections
  }`;

  return btoa(script);
}

function activateLayer(year) {
  if (activeLayers[year]) {
    deactivateLayer(year);
  }

  document.getElementById(`checkbox${year}`).checked = true;

  const layer = sentinelWMSLayer(year, thresholds[year]);
  activeLayers[year] = layer;
  map.addLayer(layer);
}

function deactivateLayer(year) {
  if (!activeLayers[year]) return;

  map.removeLayer(activeLayers[year]);
  activeLayers[year] = undefined;
}

function handleYearCheckbox(event, year) {
  if (event.target.checked) {
    let threshold = document.getElementById(`threshold${year}`).value;
    activateLayer(year, threshold);
  } else {
    deactivateLayer(year);
  }

  pushURL();
}

function handleThresholdSlider(event, year) {
  let threshold = event.target.value;
  document.getElementById(`thresholdValue${year}`).innerHTML = threshold;

  thresholds[year] = threshold;

  if (activeLayers[year]) {
    activateLayer(year);
  }

  pushURL();
}

function handleColor(event, year) {
  colors[year] = event.target.value;

  if (activeLayers[year]) {
    activateLayer(year);
  }

  pushURL();
}

function readURL() {
  let searchParams = new URLSearchParams(location.search);

  if (searchParams.has("mapX")) {
    let x = parseFloat(searchParams.get("mapX"));
    if (!Number.isNaN(x)) {
      mapX = x;
    }
  }

  if (searchParams.has("mapY")) {
    let y = parseFloat(searchParams.get("mapY"));
    if (!Number.isNaN(y)) {
      mapY = y;
    }
  }

  if (searchParams.has("zoom")) {
    let zoom = parseFloat(searchParams.get("zoom"));
    if (!Number.isNaN(zoom)) {
      mapZoom = zoom;
    }
  }

  if (map && map.getView()) {
    const view = map.getView();
    view.setCenter([Math.round(mapX), Math.round(mapY)]);
    view.setZoom(Math.round(mapZoom * 100) / 100);
  }

  if (searchParams.has("layer")) {
    searchParams.getAll("layer").forEach(layerString => {
      let splitLayerString = layerString.split("_");
      let year = splitLayerString[0];
      let threshold = splitLayerString[1];
      let color = splitLayerString[2];
      let active = splitLayerString[3] === "true";

      thresholds[year] = threshold;
      document.getElementById(`thresholdValue${year}`).innerHTML = threshold;
      document.getElementById(`threshold${year}`).value = threshold;

      colors[year] = color;
      document.getElementById(`legend${year}`).value = color;
      document.getElementById(`legend${year}`).style.setProperty('--value', color);

      if (active) {
        activateLayer(year);
      }
    });
  }
}

function generateURLParams() {
  let searchParams = new URLSearchParams();

  mapX = map.getView().getCenter()[0];
  mapY = map.getView().getCenter()[1];
  mapZoom = map.getView().getZoom();

  getYears().forEach(year => {
    searchParams.append("layer", `${year}_${thresholds[year] || thresholds.default}_${getColorForYear(year)}_${activeLayers[year] != undefined}`)
  });

  searchParams.set("mapX", mapX);
  searchParams.set("mapY", mapY);
  if (mapZoom) {
    searchParams.set("zoom", mapZoom);
  }

  return searchParams.toString();
}

function pushURL() {
  let baseUrl = `${location.protocol}//${location.host}${location.pathname}`;
  let parameter = "?" + generateURLParams();

  let url = baseUrl + parameter;

  window.history.pushState({}, window.title, url);
}

function createLegendForYear(year) {
  let yearRow = document.createElement("div");
  yearRow.classList.add("flex");

  let legendField = document.createElement("input");
  legendField.classList.add("legend-field");
  legendField.setAttribute("id", `legend${year}`);
  legendField.setAttribute("type", "color");
  legendField.value=(getColorForYear(year));
  legendField.style.setProperty('--value', getColorForYear(year)); // see https://stackoverflow.com/a/64371500
  legendField.setAttribute("oninput", `this.style.setProperty('--value', event.target.value);`);
  legendField.setAttribute("onchange", `handleColor(event, ${year});`);

  let rowName = document.createElement("span");
  rowName.innerText = `${year - 1}-${year}`;

  let checkbox = document.createElement("input");
  checkbox.setAttribute("id", `checkbox${year}`);
  checkbox.setAttribute("type", "checkbox");
  checkbox.setAttribute("onchange", `handleYearCheckbox(event, ${year});`);

  let thresholdContainer = document.createElement("div");

  let thresholdSlider = document.createElement("input");
  thresholdSlider.setAttribute("id", `threshold${year}`);
  thresholdSlider.setAttribute("type", "range");
  thresholdSlider.setAttribute("min", "-0.3");
  thresholdSlider.setAttribute("max", "0");
  thresholdSlider.setAttribute("step", "0.01");
  thresholdSlider.setAttribute("value", "-0.15");
  thresholdSlider.setAttribute("onchange", `handleThresholdSlider(event, ${year});`);
  thresholdSlider.setAttribute("style", "direction: rtl;");

  let thresholdDisplay = document.createElement("span");
  thresholdDisplay.classList.add("threshold");
  thresholdDisplay.setAttribute("id", `thresholdValue${year}`);
  thresholdDisplay.innerText = "-0.15";

  let thresholdLegend = document.createElement("div");
  thresholdLegend.classList.add("flex");
  thresholdLegend.classList.add("threshold-legend");

  let thresholdLow = document.createElement("span");
  thresholdLow.innerText = "fein";

  let thresholdHigh = document.createElement("span");
  thresholdHigh.innerText = "grob";

  thresholdLegend.appendChild(thresholdLow);
  thresholdLegend.appendChild(thresholdDisplay);
  thresholdLegend.appendChild(thresholdHigh);

  thresholdContainer.appendChild(thresholdSlider);
  thresholdContainer.appendChild(thresholdLegend);

  yearRow.appendChild(checkbox);
  yearRow.appendChild(rowName);
  yearRow.appendChild(legendField);
  yearRow.appendChild(thresholdContainer);

  document.getElementById("side").appendChild(yearRow);
}

function getYears() {
  let years = [];

  let year = 2016;
  let currentYear = new Date().getFullYear();

  while (year < currentYear) {
    years.push(year);
    year++;
  }

  // Only use current year in june or later
  if (year === currentYear) {
    if (new Date().getMonth() >= 5) {
      years.push(year);
    }
  }

  return years;
}

function getTimeParamForYear(year) {
  return `${year - 1}-04-01/${year}-08-31`;
}

function hexToSentinelColor(hexcode) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hexcode);
  return result
    ? [
      parseInt(result[1], 16) / 255,
      parseInt(result[2], 16) / 255,
      parseInt(result[3], 16) / 255,
      1
    ]
    : null;
}

function hslToHex(h, s, l) {
  l /= 100;
  const a = s * Math.min(l, 1 - l) / 100;
  const f = n => {
    const k = (n + h / 30) % 12;
    const color = l - a * Math.max(Math.min(k - 3, 9 - k, 1), -1);
    return Math.round(255 * color).toString(16).padStart(2, '0');   // convert to Hex and prefix "0" if needed
  };
  return `#${f(0)}${f(8)}${f(4)}`;
}

/**
 * Search a location using the swisstopo location search service
 * @param {Event} event the input event of the location search bar
 */
function handleLocationSearch(event) {
  const searchQuery = event.target.value;

  if (!searchQuery) {
    hideSearchResults();
    return;
  }

  const requestURL =
    "https://api3.geo.admin.ch/rest/services/api/SearchServer?searchText=" +
    encodeURI(searchQuery) +
    "&type=locations&sr=2056&limit=" +
    MAX_SEARCH_RESULTS;

  fetch(requestURL).then(response => response.json())
    .then(data => {
      const results = data.results;
      displaySearchResults(results);
    });
}

/**
 * Display the location search results in a dropdown
 * @param results an array of the search results to be displayed
 */
function displaySearchResults(results) {
  const resultsElement = document.getElementById("search-results");
  resultsElement.innerHTML = "";

  if (!results) {
    hideSearchResults();
    return;
  }

  results.forEach(function(result) {
    const resultRow = document.createElement("div");
    resultRow.classList.add("result-row");
    resultRow.innerHTML = result.attrs.label;
    resultRow.setAttribute("onclick", "handleLocationClick(" + JSON.stringify(result) + ");");

    document.getElementById("search-results").appendChild(resultRow);
  });

  resultsElement.classList.remove("hidden");
}

/**
 * Hide the search results dropdown
 */
function hideSearchResults() {
  const resultsElement = document.getElementById("search-results");
  resultsElement.classList.add("hidden");
  resultsElement.innerHTML = "";
}

/**
 * Move the map to the clicked location and set an appropriate zoom level.
 * @param result the location to move to
 */
function handleLocationClick(result) {
  const view = map.getView();
  view.setZoom(Math.min(result.attrs.zoomlevel, 10)); // Cap zoomlevel because the api sometimes returns 2^32-1 as zoomlevel
  view.setCenter([result.attrs.y, result.attrs.x]);

  hideSearchResults();
}

/**
 * Hide search results dropdown if there is a click outside the location search element
 * @param {Event} event the click event used to determine the click target
 */
function handleBodyClick(event) {
  if (!(event.target.id === "location-search-input" || event.target.classList.contains("result-row"))) {
    hideSearchResults();
  }
}

initialize();
